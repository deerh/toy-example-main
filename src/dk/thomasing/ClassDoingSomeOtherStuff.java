package dk.thomasing;

public class ClassDoingSomeOtherStuff {
    public void doSomeOtherStuff(){
        IColorProvider colorProvider = ColorProviderService.getColorProviderImpl();
        String aColor = colorProvider.getRandomColor();
        System.out.println("This is me doing some other stuff. Here is a color: " + aColor);
    }
}
