package dk.thomasing;

public class ColorProviderService {
    private static IColorProvider colorProviderImpl = new AdvancedColorProvider();

    private ColorProviderService(){}

    public static IColorProvider getColorProviderImpl(){
        return colorProviderImpl;
    }
}
