package dk.thomasing;

public class ClassDoingSomeStuff {
    public void doStuff(){
        IColorProvider colorProvider = ColorProviderService.getColorProviderImpl();
        String aColor = colorProvider.getRandomColor();
        System.out.println("I'm a class doing some stuff. Here is a color: " + aColor);
    }
}
