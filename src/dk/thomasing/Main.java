package dk.thomasing;

public class Main {

    public static void main(String[] args) {
        ClassDoingSomeStuff c1 = new ClassDoingSomeStuff();
        c1.doStuff();
        ClassDoingSomeOtherStuff c2 = new ClassDoingSomeOtherStuff();
        c2.doSomeOtherStuff();
    }

}
